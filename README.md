# Wordpress Installation
An appended extension of [Neoteric Design's housing-authority-survey-outcomes project](https://gitlab.com/neoteric-design/clients/housing-authority-survey-outcomes) readme on Gitlab, specifically for Wordpress.

## Wordpress Intro
Following is a detailed explanation of configuring Neoteric's housing widget for use on a Wordpress website. For our purposes, we won't need the `widget.html` file as this is only required if you intend to use the widget as a standalone plugin instead of integrated into an existing page on your website. 

Download the remaining files for upload to your Wordpress server. 

**Note:** We have removed a good deal of Neoteric's original testing files included in their repo as they are unneeded in our Wordpress install of the widget.

## Installation
For this specific install, we've downloaded the above files (minus the widget.html file) files into a folder called `housing-authority-outcomes-master` and have uploaded the folder to our child theme. In this case, our SFTP directory is as follows: `website-name/wp-content/themes/Avada-Child-Theme/housing-authority-survey-outcomes-master`.

## Dependencies
In order for the widget to work as expected, we need to enqueue several files into the `functions.php` file in your child theme. Below is an outline of what we have added to our `functions.php` file, with specific regulations as to which page(s) the styles and JS should be loaded to:

```
//Housing Calculator external CSS
function hook_css() {
    if ( is_page( '609' ) ) { //Housing Calculator page
    ?>
    <link href="https://surveyjs.azureedge.net/1.7.25/modern.css" type="text/css" rel="stylesheet"/> <!--Survey JS base css-->
    <?php
    }
}
add_action('wp_head', 'hook_css');

//Housing Calculator JS files and CSS
function register_page_specific_scripts() {
  if ( is_page( '609' ) ) { //Housing Calculator page
    wp_enqueue_script( 'survey_js', 'https://surveyjs.azureedge.net/1.7.25/survey.jquery.js', array('jquery'), '1.7.25', true ); //Survey JS base javascript
    wp_enqueue_script( 'widget_js', get_stylesheet_directory_uri() . '/housing-authority-survey-outcomes-master/widget.js', array( 'jquery' ), '1.0', true ); //Our survey JS
    wp_enqueue_style( 'widget_styles', get_stylesheet_directory_uri() . '/housing-authority-survey-outcomes-master/widget.css', array(), null, null ); //Our survey CSS
  }
}

add_action( 'wp_enqueue_scripts', 'register_page_specific_scripts' );
```

Notice above that we are making a call to our *child theme* directory with `get_stylesheet_directory_uri()` when enqueueing our local files.

## Embedding in your page
Use the following code to embed the widget into your page:

```
<div id="landingPage">
      <div class="content-container">
      <div class="green-container landing-content">
        <h1 class="title">Start your housing journey here</h1>
        <h3>Answers are only a few clicks away</h3>
        <p>Our Affordable Housing Calculator is a useful tool to help you identify the different types of housing and assistance that may be available to you. We’ll be asking a few questions that will provide eligibility information for our wide range of housing options.</p><p>Please keep in mind that this tool is not an official application for housing assistance, but will help inform whether you are eligible based on a few important factors. You must complete and submit an application to have your housing eligibility confirmed. The HomeFront Affordable Housing Calculator:</p>
        <ul>
          <li>
            Is not an offer for housing.
          </li>
          <li>
            Is not an application for housing. You need to apply to be considered for housing.
          </li>
          <li>Provides estimates only and is not a final determination of your eligibility for housing. When you apply, a different outcome may be calculated.</li>
          <li>Considers some of your personal circumstances and calculates potential eligibility based on available housing. The personal information you provide using this tool is not a complete list of the information required when completing an application.</li>
          <li>Does not store the information you provide. You will not provide any identifying details during this survey.</li>
        </ul>
        <p>HomeFront is here to help you find a place to call home. Let’s get started.</p>
        <button type="button" id="introBtn" class="sv-btn sv-footer__next-btn">Continue</button>
      </div>
    </div>
  </div>

<div id="widget">
  <div id="surveyNavigationTop"></div>
    <div id="surveyElement"></div>

    <div id="surveyResult"></div>
</div>
```

**Note:** Neoteric's readme mentions only using the last portion of the above code (see the How to use section below for details), but we found that including the `landingPage` div was required to ensure page load. Otherwise, the `#widget` does not load as its opacity is controlled by a button click on line `467` of `widget.js`:

```
  //Removes the landing page and reveals the widget on starting the survey.
  $("#introBtn").on("click", function(){
    $("#landingPage").css("display", "none");
    $("#widget").css("display", "block");
  })
```

## Styling
We're using a pretty basic CodeKit setup to handle the editing of the widget.scss file. Widget.scss is set up feed into main.scss which compiles a compressed widget.css file. More info on CodeKit [here.](https://codekitapp.com/help/sass/).

Here's an example of my main.scss file (yep, it's just the one line):
```
@import "widget.scss" //Housing Calculator Widget
```

You can set the compile settings directly in CodeKit. Once CodeKit has output the minified CSS to widget.css, I just copy and paste the output into the widget.css file on the server. Copying and pasting into a file opened from the server is better than just replacing the file as the Wordpress cache recognizes the copy/paste function as a cache clearing event where the replace does not register. 

**Note:** I did find that I needed to hard refresh the calculator page in Wordpress to reflect the CSS changes. 


---

# Neoteric's original housing-authority-survey-outcomes readme

## Intro

This widget collects a list of qualifications from a user, and directs them to eligible programs based off their answers. The code is based on [Survey.JS](https://surveyjs.io/), written in standard HTML and CSS, and combined with [jQuery](https://jquery.com/).


## How To Use
This widget is designed for easy implementation in any site. It can be inserted within existing HTML, or can exist on a page of its own. All data is formatted within JS objects, and inserted into JS templates. As a default, the widget takes over an entire page. To insert within an existing page instead, copy the code below, and paste into your HTML.
```
<div id="widget">
  <div id="surveyNavigationTop"></div>
    <div id="surveyElement"></div>

    <div id="surveyResult"></div>
</div>
```


## Content

### Landing Page
All HTML content can be edited, however, it is very **important to note: ID names should not be changed.** Both Survey.JS and custom JS look for these specific ID names to render functions. Therefore, ID names should not be touched. Feel free to add your custom HTML elements, however.

### Question, helper title, and helper description
Question, helper title, and helper description are contained within the helperCopy object, with the keys corresponding to the survey page names. The key names (`people`, `income`, `demo`, etc...) are best left unchanged for proper functioning. Below is an example of how content should be formatted within the helperCopy object.

```
  const helperCopy = {
    "people":
      {
        question: "How many people are in your household?",
        title: "You and your family",
        description: "At least 1 member of your household must be 18 years or older to apply for services."
      },
    "income":
      {
        question: "What is your estimated annual household income?",
        title: "Your income counts",
        description: "To qualify for housing assistance, your family’s income must be below a certain amount. Your annual household income includes all yearly salaries, bonuses, commissions, overtime, tips and other income sources from all family members over 18 — before any deductions. Income limits differ based on the number of people in your household. We’ll help with the details when you apply."
      }
    }
  ```

### Income Levels
The Income level object contains keys ranging from 1-8, and their pairing values are a set of 4 income levels. The keys correspond to the number of people within a household-- so key '2' equates to 2 persons within the household. Key numbers and their paired income levels can be changed, but if so, it is important to update the `qualifiesFor` array within each income level. Here is the definition for the `qualifiesFor` terminology:
* `public` = "Public Housing"
* `affordable` = "Affordable Housing"
* `hcv` = "Housing Choice Voucher"
* `senior` = "Senior Housing"
* `accessible` = "Accessible Housing"
* `supportive` = "Supportive Housing"

The Object key, income levels, and qualifications array are all contained within the `incomeLevels` object, and should be formatted like below:

Qualifications Terms:

```
const incomeLevels = {
  ...

  '2': [
    { income: "$50 to $18,400", qualifiesFor: ["public", "affordable", "hcv"] },
    { income:   "$18,401 to $30,650", qualifiesFor: ["affordable", "hcv"] },
    { income:    "$30,651 to $49,050", qualifiesFor: ["hcv"] },
    { income:   "$49,051+", qualifiesFor: [] }
  ],
}
```

### Results Link list
The results page renders the qualified programs from the `allPrograms` object. This object includes a qualifications term (defined above) as a key, and a paired object containing a custom program label, program description, and button url. Programs can be added, removed, or edited. If adding a program, make sure to register it within one of the `qualifiesFor` arrays described above. 

```
const allPrograms = {
  "public":  {
    label: "Public Housing",
    providers: [
      {
        description: "Homes provided in one of HUD’s housing communities.",
        url: "http://www.google.com"
      }
    ]
  },

  "affordable":  {
    label: "Affordable Housing",
    providers: [
      {
        description: "Homes provided in one of our housing communities.",
        url: "http://www.google.com"
      }
    ]
  },

  "hcv":  {
    label: "Housing Choice Voucher",
    providers: [
      {
        description: "Households are provided with a rent subsidy that helps you afford rent at the residence of your choice.",
        url: "http://www.google.com"
      }
    ]
  },
}
```


## Styles

Only standard CSS is used for this application. Therefore, all styles are customizable. For some convenience, the primary color, along with primary font family are abstracted in CSS variables and placed at the top of the file. Feel free to add additional CSS for personalization.

### Deactivate Fade-In
To remove the Fade-in effect, comment out the `document.ready function()` within the JS file.
```
$(document).ready(() => {
  $("main").animate({ opacity: 1 }, 700)

  $(".sv-btn.sv-footer__next-btn, .sv-btn.sv-footer__complete-btn").each(function(index) {
    let el = $(this);
    $(el).on("click", function(){
      $("main").css("opacity", 0);
      $("main").animate({ opacity: 1 }, 700)
    })
  })

})
```
Then, in the CSS file, under the `main` selector, remove the `opacity` property. Fade-In should now be deactivated.


## Further Development
For questions or inquiries for further improvement, please contact the development team, [Neoteric Design](https://www.neotericdesign.com/)

### Testing

WIP. Open `test/test.html` in a browser to execute the tests and see the results.