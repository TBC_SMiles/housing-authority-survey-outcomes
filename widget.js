//Income ranges corresponding to the number of people in household
const incomeLevels = {
  '1': [
    { income: "$50 to $16,100", qualifiesFor: ["public", "affordable", "hcv"] },
    { income: "$16,101 to $26,850", qualifiesFor: ["affordable", "hcv"] },
    { income:  "$26,851 to $42,950", qualifiesFor: ["hcv", "public"] },
    { income:   "$42,951+", qualifiesFor: [] }
  ],
  '2': [
    { income: "$50 to $18,400", qualifiesFor: ["public", "affordable", "hcv"] },
    { income:   "$18,401 to $30,650", qualifiesFor: ["affordable", "hcv"] },
    { income:    "$30,651 to $49,050", qualifiesFor: ["hcv", "public"] },
    { income:   "$49,051+", qualifiesFor: [] }
  ],
  '3': [
    { income: "$50 to $21,720", qualifiesFor: ["public", "affordable", "hcv"] },
    { income: "$21,721 to $34,500", qualifiesFor: ["affordable", "hcv"] },
    { income: "$34,501 to $55,200", qualifiesFor: ["hcv", "public"] },
    { income: "$55,201+", qualifiesFor: [] },
  ],
  '4': [
    { income: "$50 to $26,200", qualifiesFor: ["public", "affordable", "hcv"] },
    { income: "$26,201 to $38,300", qualifiesFor: ["affordable", "hcv"] },
    { income: "$38,301 to $61,300", qualifiesFor: ["hcv", "public"] },
    { income: "$61,300+", qualifiesFor: [] },
  ],
  '5': [
    { income: "$50 to $30,680", qualifiesFor: ["public", "affordable", "hcv"] },
    { income: "$30,681 to $41,400", qualifiesFor: ["affordable", "hcv"] },
    { income: "$41,401 to $66,250", qualifiesFor: ["hcv", "public"] },
    { income: "$66,251+", qualifiesFor: [] },
  ],
  '6': [
    { income: "$50 to $35,160", qualifiesFor: ["public", "affordable", "hcv"] },
    { income: "$35,161 to $44,450", qualifiesFor: ["affordable", "hcv"] },
    { income: "$44,451 to $71,150", qualifiesFor: ["hcv", "public"] },
    { income: "$71,151+", qualifiesFor: [] },
  ],
  '7': [
    { income: "$50 to $39,640", qualifiesFor: ["public", "affordable", "hcv"] },
    { income: "$39,641 to $47,500", qualifiesFor: ["affordable", "hcv"] },
    { income: "$47,501 to $76,050", qualifiesFor: ["hcv", "public"] },
    { income: "$76,050+", qualifiesFor: [] },
  ],
  '8': [
    { income: "$50 to $44,120", qualifiesFor: ["public", "affordable", "hcv"] },
    { income: "$44,121 to $50,600", qualifiesFor: ["affordable", "hcv"] },
    { income: "$50,600 to $80,950", qualifiesFor: ["hcv", "public"] },
    { income: "$80,951+", qualifiesFor: [] },
  ]
}

// Customizable HTML for your local resources. Displayed when no qualified programs found.
const localResourcesHTML = `<a href="/community-resources/">Local Resources</a>`


//All questions, helper title, and helper description for survey panels
const helperCopy = {
  "people":
    {
      question: "How many people are in your household?",
      title: "You and your family",
      description: "At least 1 member of your household must be 18 years or older to apply for services."
    },
  "income":
    {
      question: "What is your estimated annual household income?",
      title: "Your income counts",
      description: "To qualify for housing assistance, your family’s income must be below a certain amount. Your annual household income includes all yearly salaries, bonuses, commissions, overtime, tips and other income sources from all family members over 18 — before any deductions. Income limits differ based on the number of people in your household. We’ll help with the details when you apply."
    },
    "demo":
    {
      question: "Is a member of your household in need of wheelchair-accessible housing? Fully accessible?",
      title: "Find the right housing fit",
      description: "Housing help comes in all sizes and types, from scattered single-family houses to high-rise apartments. Tell us a little about yourself and if you qualify as elderly, a person with a disability or a family."
    },
    "circum":
    {
      question: "Are you currently a victim of domestic violence, homeless, or a Veteran experiencing homelessness?",
      title: "Your situation matters",
      description: "Giving preference to specific groups enables us to direct limited housing resources to the families with the greatest needs. Reach out, and we’ll do our best to help you find the resources you need."
    },
    "submit":
    {
      title: "Submit",
      description: "Check to see all answers are correct. When finished, press complete"
    },
    "result":
    {
      title: "The next step on your journey",
      description: "HomeFront is here to help — you may qualify for the housing solution(s) listed below. Find out more by applying online or contacting us. We have answers to your questions. Let’s continue the journey together."
    }
};


//Link list on results page. Including category name, category description, and link url
const allPrograms = {
  "public":  {
    label: "Public Housing",
    providers: [
      {
        description: "Homes provided in one of HUD’s housing communities.",
        url: "/communities/public-housing-rentals/"
      }
    ]
  },

  "affordable":  {
    label: "Affordable Housing",
    providers: [
      {
        description: "Homes provided in one of our housing communities.",
        url: "/our-homes/"
      }
    ]
  },

  "hcv":  {
    label: "Housing Choice Voucher",
    providers: [
      {
        description: "Households are provided with a rent subsidy that helps you afford rent at the residence of your choice.",
        url: "/housing-choice-voucher-program/"
      }
    ]
  },

  "senior":  {
    label: "Senior Housing",
    providers: [
      {
        description: "Independent living options for seniors.",
        url: "/our-homes/#62-senior-rentals"
      }
    ]
  },

  "accessible":  {
    label: "Accessible Housing",
    providers: [
      {
        description: "Communities for persons with disabilities.",
        url: "/accessible-rentals/"
      }
    ]
  },

  "supportive":  {
    label: "Community Resources",
    providers: [
      {
        description: "Contact us or get in touch with one of the many housing support agencies in the Billings area.",
        url: "/community-resources/"
      }
    ]
  },
  // "pvwr":  {
  //   label: "Preferential Voucher, with Referral",
  //   providers: [
  //     {
  //       description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque inventore ipsum aspernatur iusto laudantium! Fugiat praesentium quos voluptas aliquam explicabo numquam doloremque quia, distinctio beatae omnis dolorum maxime perferendis porro?",
  //       url: "www.apple.com"
  //     }
  //   ]
  // },
}

// Display programs list in the surveyResult container
function renderResults(result) {
  const preferentialPrograms = evaluatePreferential(result.data)
  const incomePrograms = evaluateIncome(result.data)
  let resultsHTML

  if([...preferentialPrograms, ...incomePrograms].length > 0 ) {
      resultsHTML =  `<div class="results-page">
        <div class="results-header">
          <h3>The next step on your journey</h3>

          HomeFront is here to help — you may qualify for the housing solution(s) listed below. Find out more by <a href="/apply-now/" target="_blank">applying</a> or <a href="/about-homefront/contact-us/" target="_blank">contacting us.</a> We have answers to your questions. Let’s continue the journey together.
        </div>

        <div>
          ${renderProgramList(incomePrograms)}
        </div>
        <div>
          ${renderProgramList(preferentialPrograms)}
        </div>
      </div>
      `
  } else {
      resultsHTML = `<div class="results-page">
        <div class="results-header">
          <h3>The next step on your journey</h3>

          Based on your responses, you do not qualify for subsidized housing. But HomeFront is here to help. No matter your situation, we will work with you and help you overcome your obstacles to finding and keeping a home. We have answers to your questions. <a href="/about-homefront/contact-us/" target="blank">Contact us today,</a> and let’s continue the journey together.
        </div>
        <!--<div>
          ${localResourcesHTML}
        </div>
      </div>-->
      `
  }

  document
    .querySelector('#surveyResult')
    .insertAdjacentHTML('beforeend', resultsHTML)
}

// Generate HTML for an array of programs
function renderProgramList(programs) {
  return programs.map(renderProgram).join("")
}

// Generate HTML for a single program
function renderProgram(program) {
  if(!program) { return "" }
  const providersList = program.providers.map(renderProgramProvider).join("")
  return `<div class="landing-content results"><h3 class="title">${program.label}</h3>${providersList}</div>`
}

// Generate HTML for a single provider
function renderProgramProvider(provider) {
  return `<div><p>${provider.description}</p><a class="sv-btn sv-footer__next-btn no-underline" href="${provider.url}">Learn More <span class="arrow"> &#8594;</span></a></div><br><br>`
}

// Returns array of programs qualified for based on income survey answers
function evaluateIncome(surveyData) {
  return incomeLevels[surveyData.minors]
           .find(value => value.income === surveyData.income)['qualifiesFor']
           .map((programName) => allPrograms[programName])
}

// Returns array of programs qualified for based on preferential survey answers
function evaluatePreferential(surveyData) {
  let results = []

  if(surveyData['under_seventeen'] === false && surveyData['over_sixty_two'] === true) {
    results.push(allPrograms.senior)
  }

  if(surveyData["needs_wheelchair_accessibility"] === true) {
     results.push(allPrograms.accessible)
  }

  const circumstance = surveyData['circumstance'] || []

  if(circumstance.filter(x => ["homeless", "veteran_homeless"].includes(x)).length > 0) {
    results.push(allPrograms.supportive)
  }

  if(circumstance.length > 0) {
    results.push(allPrograms.pvwr)
  }

  return results
}

// Sets income bracket choices based on number in household
function setIncomeChoices(numMinors) {
  let incomeQuestion = survey.getQuestionByName("income")
  incomeQuestion.clearValue()
  incomeQuestion.clearErrors()
  incomeQuestion.choices = incomeLevels[numMinors].map(value => value['income']);
}


// Configuration for question and answer selection
const surveyConfig = {
  goNextPageAutomatic: false,
  showNavigationButtons: true,
  pages: [
    {
      name: "People",
      navigationTitle: "People",
      elements: [
            //question and answer
            {
                  type: "dropdown",
                  name: "minors",
                  title: helperCopy.people.question,
                  isRequired: true,
                  "choicesOrder": "asc",
                  choices: Object.keys(incomeLevels)
                },

                //helper copy template
            {
                type: "panel",
                name: "Additional Info",
                elements: [
                    {
                        "type": "html",
                        "name": "people_copy",
                        "html": `<article class='helper-copy green-container'>    <h4 class=''>${helperCopy.people.title}</h4> <div class=''>  <p> ${helperCopy.people.description} \t\t\t</p>  </div> </article>`
                    }
                ],
            }
        ]
    },

    {
      name: "Income",
      navigationTitle: "Income",
      elements: [
        //question and answer radiogroup
        {
          type: "radiogroup",
          name: "income",
          title: helperCopy.income.question,
          isRequired: true,
          choices: []
        },

        //helper copy template
                {
            type: "panel",
            name: "Additional Info",
            elements: [
                {
                    "type": "html",
                    "name": "Income_copy",
                    "html": `<article class='helper-copy green-container'>    <h4 class=''>${helperCopy.income.title}</h4>    <div class=''>  <p> ${helperCopy.income.description} \t\t\t</p>  </div> </article>`
                }
            ],
        }
      ]
    },

    {
      name: "Demographics",
      navigationTitle: "Demographics",
      elements: [
      //question and answer booleans
          {
            type: "panel",
            elements: [
              {
                type: "panel",
                name: "Over_62_panel",
                elements: [
                    {
                        type: "boolean",
                        name: "over_sixty_two",
                        titleLocation: "hidden",
                        labelTrue: "Yes",
                        labelFalse: "No",
                        "hideNumber": true
                    }
                ],
                title: "Are any members of your household 62 or older?",
                isRequired: true,
                showNumber: true
            },
            {
              type: "panel",
              name: "Under_17_panel",
              elements: [
                  {
                      type: "boolean",
                      name: "under_seventeen",
                      titleLocation: "hidden",
                      labelTrue: "Yes",
                      labelFalse: "No",
                      "hideNumber": true
                  }
              ],
              title: "Are any members of your household 17 or younger?",
              isRequired: true,
              showNumber: true
          },

          {
            type: "panel",
            name: "accessible_panel",
            elements: [
                {
                    type: "boolean",
                    name: "accessible",
                    titleLocation: "hidden",
                    labelTrue: "Yes",
                    labelFalse: "No",
                    "hideNumber": true
                }
            ],
            title: helperCopy.demo.question,
            isRequired: true,
            showNumber: true
          }
            ],
          },

          //helper copy
          {
            type: "panel",
            name: "Additional Info",
            elements: [
              {
                  "type": "html",
                  "name": "demo",
                  "html": `<article class='helper-copy green-container'>    <h4 class=''>${helperCopy.demo.title}</h4>    <div class=''>  <p> ${helperCopy.demo.description} \t\t\t</p>  </div> </article>`
              }
            ],
          }
        ]
      },


    {
      name: "Circumstances",
      navigationTitle: "Circumstances",
      elements: [

          {
            type: "panel",
            elements: [
            //question and answer checkboxes
              {
                type: "checkbox",
                name: "circumstance",
                title: helperCopy.circum.question,
                isRequired: false,
                choices: [
                  {text: "Victim of domestic violence", value: "vdv" } ,
                  {text: "Homeless", value: "homeless"},
                  {text: "Veteran experiencing homelessness", value: "veteran_homeless"}
                ],
              },
            ],
          },
          //helper copy template
        {
            type: "panel",
            name: "Additional Info",
            elements: [
                {
                    "type": "html",
                    "name": "Circumstances",
                    "html": `<article class='helper-copy green-container'>    <h4 class=''>${helperCopy.circum.title}</h4>    <div class=''>  <p> ${helperCopy.circum.description} \t\t\t</p>  </div> </article>`
                }
            ]
          }
      ]
    },
  ],
  triggers: [
    {
        type: "runExpression",
        expression: "{minors} notempty or {minors} empty",
        runExpression: "setIncomeChoices({minors})"
    }
  ]
}
//replaces all instances of $ with jquery for Wordpress
jQuery(document).ready(function($) {
$(document).ready(() => {
  //Fade in animation. To deactivate, comment out this section, and remove the opacity property from the "main" selector in CSS.
  $("main").animate({ opacity: 1 }, 700)

  $(".sv-btn.sv-footer__next-btn, .sv-btn.sv-footer__complete-btn").each(function(index) {
    let el = $(this);
    $(el).on("click", function(){
      $("main").css("opacity", 0);
      $("main").animate({ opacity: 1 }, 700)
    })
  })

  //Removes the landing page and reveals the widget on starting the survey.
  $("#introBtn").on("click", function(){
    $("#landingPage").css("display", "none");
    $("#widget").css("display", "block");
  })

  //base theme provided by survey.js
  Survey
      .StylesManager
      .applyTheme("modern")

  Survey
      .Serializer
      .addProperty("page", {
          name: "navigationTitle:string"
      });

  Survey
      .FunctionFactory
      .Instance
      .register("setIncomeChoices", setIncomeChoices)

  window.survey = new Survey.Model(surveyConfig);

  //Render Results page after survey is complete
  survey
    .onComplete
    .add(renderResults)

  //mount survey
  $("#surveyElement").Survey({model: survey});

  initializeNavigationBar(survey)
})


//NAVIGATION BAR
function initializeNavigationBar(survey) {
  var navTopEl = document.querySelector("#surveyNavigationTop");
  navTopEl.className = "navigationContainer";

  var navProgBarDiv = document.createElement("div");
  navProgBarDiv.className = "navigationProgressbarDiv";
  navTopEl.appendChild(navProgBarDiv);

  var navProgBar = document.createElement("ul");
  navProgBar.className = "navigationProgressbar";
  navProgBarDiv.appendChild(navProgBar);

  var liEls = [];

  for (var i = 0; i < survey.visiblePageCount; i++) {
      var liEl = document.createElement("li");
      if (survey.currentPageNo == i) {
          liEl
              .classList
              .add("current");
      }
      liEl.onclick = function (index) {
          return function () {
              if (survey['isCompleted'])
                  return;
              liEls[survey.currentPageNo]
                  .classList
                  .remove("current");
              if (index < survey.currentPageNo) {
                  survey.currentPageNo = index;
              } else if (index > survey.currentPageNo) {
                  var j = survey.currentPageNo;
                  for (; j < index; j++) {
                      if (survey.visiblePages[j].hasErrors(true, true))
                          break;
                      if (!liEls[j].classList.contains("completed")) {
                          liEls[j]
                              .classList
                              .add("completed");
                      }
                  }
                  survey.currentPageNo = j;
              }
              liEls[survey.currentPageNo]
                  .classList
                  .add("current");
          };
      }(i);
      var pageTitle = document.createElement("span");
      if (!survey.visiblePages[i].navigationTitle) {
          pageTitle.innerText = survey
              .visiblePages[i]
              .name;
      } else
          pageTitle.innerText = survey
              .visiblePages[i]
              .navigationTitle;
      pageTitle.className = "pageTitle";
      liEl.appendChild(pageTitle);
      var br = document.createElement("br");
      liEl.appendChild(br);
      var pageDescription = document.createElement("span");
      if (!!survey.visiblePages[i].navigationDescription) {
          pageDescription.innerText = survey
              .visiblePages[i]
              .navigationDescription;
      }
      pageDescription.className = "pageDescription";
      liEl.appendChild(pageDescription);
      liEls.push(liEl);
      navProgBar.appendChild(liEl);
  }

  survey
      .onCurrentPageChanged
      .add(function (sender, options) {
          var oldIndex = options.oldCurrentPage.visibleIndex;
          var newIndex = options.newCurrentPage.visibleIndex;
          liEls[oldIndex]
              .classList
              .remove("current");
          if (newIndex > oldIndex) {
              for (var i = oldIndex; i < newIndex; i++) {
                  if (sender.visiblePages[i].hasErrors(true, true))
                      break;
                  if (!liEls[i].classList.contains("completed")) {
                      liEls[i]
                          .classList
                          .add("completed");
                  }
              }
          }
          liEls[newIndex]
              .classList
              .add("current");
      });
  survey
    .onComplete
    .add(function (sender, options) {
        liEls[sender.currentPageNo]
            .classList
            .remove("current");
        for (var i = 0; i < sender.visiblePageCount; i++) {
            if (survey.visiblePages[i].hasErrors(true, true))
                break;
            if (!liEls[i].classList.contains("completed")) {
                liEls[i]
                    .classList
                    .add("completed");
            }
        }
    });
}
    });